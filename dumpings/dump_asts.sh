#!/bin/sh

rm out/*

for f in `ls in/*.cpp`
do
  echo "Processing $f"
  clang -Xclang -ast-dump -fsyntax-only "$f" -fno-color-diagnostics > out/$(basename "$f") 
  echo "    $f done" && echo
done
